<!DOCTYPE html>
<html>
  <head>
    <title>Register</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  <br />
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  @auth
    <div class="container box">
      <h3 align="center">Register</h3><br />
      <form method="post" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="form-group">
          <label>Email</label>
            <input type="email" name="email" class="form-control" />
        </div>
        <div class="form-group">
          <label>Username</label>
            <input type="text" name="name" class="form-control" />
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" />
        </div>
        <div class="form-group">
          <label>Password Confirmation</label>
          <input type="password" name="password_confirmation" class="form-control" />
        </div>
        <div class="form-group">
          <label>Role</label>
          <select name="role" class="form-control">
            @if(Auth::user()->role == '2')
              <option value="3">Admin</option>
            @endif
            <option value="4">Agent</option>
          </select>
        </div>
        <div class="form-group">
          <input type="submit" name="register" class="btn btn-primary" value="Register" />
        </div>
      </form>
    </div>
  @endauth

  @guest
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest  
  </body>
</html>
