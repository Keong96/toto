<!DOCTYPE html>
<html>
  <head>
    <title>Agent List</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
    @if(Auth::user()->role == '2' || Auth::user()->role == "3")
    <div class="container box">
      <h3 align="center">Agent List</h3><br/>
      <table class="table table-striped">
        <tr>
          <th>Name</th>
          <th>Package</th>
          <th>Amount</th>
          <th>Action</th>
        </tr>
        @foreach ($agents as $agent)
        <tr>
          <td><a href="{{url('/edit-agent/')}}/{{$agent->id}}">{{ $agent->name }}</a></td>
          <td>{{ $agent->package_name }}</td>
          <td>123</td>
          <td><input class="btn btn-danger" type="button" name="archive" value="Archive"/></td>
        </tr>
        @endforeach
      </table>
      <br />
    </div>
    @else
      <script>alert('Unauthorized');</script>
      <script>window.location.href = "{{url('/home')}}";</script>
    @endelse
    @endif
  @endauth  
  </body>
</html>
