<!DOCTYPE html>
<html>
  <head>
    <title>Edit Package</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  @if(Auth::user()->role == '2' || Auth::user()->role == "3")
  <div class="container box">
    <h3 align="center">{{ $package->name }}</h3><br />
    <form method="post" action="{{ url('/update-package') }}">
      {{ csrf_field() }} 
      <div class="form-group">
        <input type="hidden" name="package_id" class="form-control" value="{{ $package->id }}"/>
        Package Name: <input type="text" class="form-control" name="package_name" value="{{ $package->name }}"/><br>
        Commision Rate: <input type="number" step=0.01 min=0 max=100 class="form-control" name="commission_rate" value="{{ $package->commission_rate }}"/><br>
        Bonus Rate: <input type="number" step=0.01 min=0 max=100 class="form-control" name="bonus_rate" value="{{ $package->bonus_rate }}"/><br>
      </div>
      <input type="submit" name="update" class="btn btn-success" value="Update" />
      <br><br>
    </form>
  </div>
  @endif
  @endauth
  <br />
  </body>
</html>
