<!DOCTYPE html>
<html>
  <head>
    <title>Main Page Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:800px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  <div class="container box">
    <h3 align="center">Main Page</h3><br />
      <strong>Welcome {{ Auth::user()->name }}</strong>
      <br /><br />
      <span><input type="button" name="logout" class="btn btn-danger" value="Logout" onclick='window.location.href = "{{url('/logout')}}"'/></span>
      @if(Auth::user()->role == '2' || Auth::user()->role == "3")
        <span><input type="button" name="agent" class="btn btn-primary" value="Agents" onclick='window.location.href = "{{url('/agent')}}"'/></span>
        <span><input type="button" name="package" class="btn btn-success" value="Packages" onclick='window.location.href = "{{url('/package')}}"'/></span>
        <span>          <input type="button" name="register" class="btn btn-warning" value="Register" onclick='window.location.href = "{{url('/new-user')}}"'/></span>
      @endif
  </div>
  <br />
  @if(Auth::user()->role == '4')
  <div class="container box">
    <h3 align="center">Your Settlement</h3><br />
    <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Draw Date</th>
          <th scope="col">Number</th>
          <th scope="col">Type</th>
          <th scope="col">Amount</th>
          <th scope="col">Platform</th>
          <th scope="col">Commission</th>
          <th scope="col">Bonus</th>
          <th scope="col">Settlement Date</th>
        </tr>
        @foreach ($records as $index => $record)
        <tr>
          <td scope="row">{{ $index + 1 }}</td>
          <td>{{ $record->draw_date }}</td>
          <td>{{ $record->number }}</td>
          <td>{{ $record->type }}</td>
          <td>{{ $record->amount }}</td>
          <td>{{ $record->platform }}</td>
          <td>{{ $record->commission }}</td>
          <td>{{ $record->bonus }}</td>
          <td>{{ $record->settlement }}</td>
        </tr>
        @endforeach
      </table>
  </div>
  @endif
  @endauth  
  </body>
</html>
