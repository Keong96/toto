<!DOCTYPE html>
<html>
  <head>
    <title>Package Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  @if(Auth::user()->role == '2' || Auth::user()->role == "3")
    <div class="container box">
      <h3 align="center">Packages</h3><br />
      <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Package name</th>
          <th scope="col">Commision Rate</th>
          <th scope="col">Bonus Rate</th>
        </tr>
        @foreach ($packages as $index => $package)
        <tr>
          <td scope="row">{{ $index + 1}}</td>
          <td><a href="{{ url('/edit-package') }}/{{ $package->id }}">{{ $package->name }}</a></td>
          <td>{{ $package->commission_rate }}%</td>
          <td>{{ $package->bonus_rate }}%</td>
        </tr>
        @endforeach
      </table>
      <br />
    </div>
    <br />
    <div class="container box">
      <h4>Create New Package</h4>
      <form method="post" action="{{ url('/create-package') }}">
        {{ csrf_field() }} 
        <div class="form-group">
          <table class="table">
            <tr>
              <td><input type="text" name="package_name" class="form-control" placeholder="Package Name" /></td>
              <td><input type="number" min=0 max=100 step=0.01 name="commission_rate" class="form-control" placeholder="Commission Rate"/></td>
              <td><input type="number" min=0 max=100 step=0.01 name="bonus_rate" class="form-control" placeholder="Bonus Rate" /></td>
              <td><input type="submit" name="create" class="btn btn-primary" value="Create" /></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  @else
    <script>alert('Unauthorized');</script>
    <script>window.location.href = "{{url('/home')}}";</script>
  @endelse
  @endif
  @endauth  
  </body>
</html>
