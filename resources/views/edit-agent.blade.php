<!DOCTYPE html>
<html>
  <head>
    <title>Edit Agent</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  @if(Auth::user()->role == '2' || Auth::user()->role == "3")
  <div class="container box">
    <h3 align="center">{{$agent->name}}</h3><br />
    <form method="post" action="{{ url('/update-agent') }}">
      {{ csrf_field() }}
      <input type="hidden" name="user_id" class="form-control" value="{{ $agent->id }}"/>
      Current Package:
      <select name="package" class="form-control">
        @foreach($packages as $package)
          @if($agent->package_id == $package->id)
            <option value="{{ $package->id }}" selected>{{ $package->name }}</option>
          @endif
          <option value="{{ $package->id }}">{{ $package->name }}</option>
        @endforeach
      </select>
      <br /><input type="submit" name="Submit" class="btn btn-success" value="Submit"/>
    </form>
  </div>
  @endif
  @endauth
  <br />
  </body>
</html>
