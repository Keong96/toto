<!DOCTYPE html>
<html>
  <head>
    <title>Admin Transaction Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  @if(Auth::user()->role == '2' || Auth::user()->role == '3')
    <div class="container box">
      <h3 align="center">Transaction Record</h3><br />
      <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Draw Date</th>
          <th scope="col">Number</th>
          <th scope="col">Agent</th>
          <th scope="col">Type</th>
          <th scope="col">Amount</th>
          <th scope="col">Platform</th>
        </tr>
        @foreach ($transactions as $index => $transaction)
        <tr>
          <td scope="row">{{ $index + 1 }}</td>
          <td>{{ $transaction->draw_date }}</td>
          <td>{{ $transaction->number }}</td>
          <td>{{ $transaction->agent_name }}</td>
          <td>{{ $transaction->type }}</td>
          <td>{{ $transaction->amount }}</td>
          <td>{{ $transaction->platform }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    <br />
    <div class="container box">
      <h3 align="center">Historical Record</h3><br />
      <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Draw Date</th>
          <th scope="col">Number</th>
          <th scope="col">Agent</th>
          <th scope="col">Type</th>
          <th scope="col">Amount</th>
          <th scope="col">Platform</th>
        </tr>
        @foreach ($histories as $index => $history)
        <tr>
          <td scope="row">{{ $index + 1 }}</td>
          <td>{{ $history->draw_date }}</td>
          <td>{{ $history->number }}</td>
          <td><a href="edit-agent/{{ $history->agent_id }}">{{ $history->agent_name }}</a></td>
          <td>{{ $history->type }}</td>
          <td>{{ $history->amount }}</td>
          <td>{{ $history->platform }}</td>
        </tr>
        @endforeach
      </table>
    </div>
  @endif
  @endauth  
  </body>
</html>
