<!DOCTYPE html>
<html>
  <head>
    <title>Transaction Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
      .box{
        width:600px;
        margin:0 auto;
        border:1px solid #ccc;
      }
    </style>
  </head>
  <body>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
  <br />
  @guest
    <script>alert('Please Login');</script>
    <script>window.location.href = "{{url('/login')}}";</script>
  @endguest

  @auth
  @if(Auth::user()->role == '4')
    <div class="container box">
      <h3 align="center">Transaction Record</h3><br />
      <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Draw Date</th>
          <th scope="col">Number</th>
          <th scope="col">Type</th>
          <th scope="col">Amount</th>
          <th scope="col">Platform</th>
        </tr>
        @foreach ($transactions as $index => $transaction)
        <tr>
          <td scope="row">{{ $index + 1 }}</td>
          <td>{{ $transaction->draw_date }}</td>
          <td>{{ $transaction->number }}</td>
          <td>{{ $transaction->type }}</td>
          <td>{{ $transaction->amount }}</td>
          <td>{{ $transaction->platform }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    <br />
    <div class="container box">
      <h3 align="center">Historical Record</h3><br />
      <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Draw Date</th>
          <th scope="col">Number</th>
          <th scope="col">Type</th>
          <th scope="col">Amount</th>
          <th scope="col">Platform</th>
        </tr>
        @foreach ($histories as $index => $history)
        <tr>
          <td scope="row">{{ $index + 1 }}</td>
          <td>{{ $history->draw_date }}</td>
          <td>{{ $history->number }}</td>
          <td>{{ $history->type }}</td>
          <td>{{ $history->amount }}</td>
          <td>{{ $history->platform }}</td>
        </tr>
        @endforeach
      </table>
    </div>
    <br />
    <div class="container box">
      <h4>Create Transaction</h4><br />
      <form method="post" action="{{ url('/create-transaction') }}">
        {{ csrf_field() }} 
        <div class="form-group">
          <label>Number</label><input required type="number" min=0 max=9999 name="number" class="form-control" placeholder="eg:1234" />
          <label>Type</label><input required type="text" name="type" class="form-control" placeholder="BIG / SMALL / A" />
          <label>Amount</label><input required type="number" min=0 step=0.01 name="amount" class="form-control" placeholder="RM" />
          <label>Platform</label><input required type="string" name="platform" class="form-control" placeholder="Magnum / Toto / Damacai" />
          <label>Draw Date</label><input required type="date" name="draw_date" class="form-control"/>
        </div>
        <input type="submit" name="create" class="btn btn-primary" value="Create" /><br><br>
      </form>
    </div>
  @endif
  @endauth  
  </body>
</html>
