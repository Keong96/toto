<?php

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
			'name' => 'Package A',
			'commission_rate' => 5.00,
			'bonus_rate' => 20.00
        ]);

        DB::table('packages')->insert([
			'name' => 'Package B',
			'commission_rate' => 25.00,
			'bonus_rate' => 15.00
        ]);

        DB::table('packages')->insert([
			'name' => 'Package C',
			'commission_rate' => 3.00,
			'bonus_rate' => 10.00
        ]);
    }
}
