<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'developer',
            'email' => 'developer@email.com',
            'password' => Hash::make('password'),
            'role' => 1,
            'package_id' => null
        ]);

        DB::table('users')->insert([
            'name' => 'superadmin',
            'email' => 'superadmin@email.com',
            'password' => Hash::make('123'),
            'role' => 2,
            'package_id' => null
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@email.com',
            'password' => Hash::make('123'),
            'role' => 3,
            'package_id' => null
        ]);

        DB::table('users')->insert([
            'name' => 'agent',
            'email' => 'agent@email.com',
            'password' => Hash::make('123'),
            'role' => 4,
            'package_id' => null
        ]);
    }
}
