<?php

use Illuminate\Database\Seeder;

class TransactionRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_records')->insert([
			'draw_date' => '2020-02-13',
			'number' => 1234,
			'user_id' => 4,
			'type' => 'SMALL',
			'amount' => 20.00,
			'platform' => 'Magnum',
			'result' => 'First',
			'commisson' => 0.2,
			'bonus' => 150.50,
			'settlement' => '2020-02-14 00:00:00'
        ]);

        DB::table('transaction_records')->insert([
			'draw_date' => '2020-03-13',
			'number' => 5678,
			'user_id' => 4,
			'type' => 'BIG',
			'amount' => 21.50,
			'platform' => 'Toto',
			'result' => 'Special',
			'commisson' => 0.2,
			'bonus' => 150.50,
			'settlement' => '2020-02-14 00:00:00'
        ]);

        DB::table('transaction_records')->insert([
			'draw_date' => '2020-04-13',
			'number' => 9012,
			'user_id' => 4,
			'type' => 'A',
			'amount' => 100.50,
			'platform' => 'Damacai',
			'result' => 'None',
			'commisson' => 0.2,
			'bonus' => 150.50,
			'settlement' => '2020-02-14 00:00:00'
        ]);
    }
}
