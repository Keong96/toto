<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'name', 'commission_rate', 'bonus_rate'
    ];  
    
    protected $hidden = [
    ];
}
