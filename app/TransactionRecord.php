<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionRecord extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id', 'number', 'user_id', 'type', 'amount', 'platform', 'draw_date', 'result', 'commisson', 'bonus', 'settlement'
    ];  
    
    protected $hidden = [
    ];
}
