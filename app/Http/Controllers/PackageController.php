<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;

class PackageController extends Controller
{

	public function index()
	{
		$packages = Package::all();

		return view('package', compact('packages'));
	}

	public function create(Request $request)
	{
		$this->validate($request, [
			'package_name'   => 'required',
			'commission_rate'  => 'required|numeric|between:0,100',
			'bonus_rate'  => 'required|numeric|between:0,100'
		]);

		Package::create([
			'name' => $request->get('package_name'),
			'commission_rate' => $request->get('commission_rate'),
			'bonus_rate' => $request->get('bonus_rate')
		]);

		return redirect('/package');
	}

	public function edit(Package $package)
	{
		return view('edit-package', compact('package'));
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'package_name'   => 'required',
			'commission_rate'  => 'required|numeric|between:0,100',
			'bonus_rate'  => 'required|numeric|between:0,100'
		]);

		$package = Package::find($request->get('package_id'));
		$package->name = $request->get('package_name');
		$package->commission_rate = $request->get('commission_rate');
		$package->bonus_rate = $request->get('bonus_rate');
		$package->save();

		return redirect('/package');
	}
}
