<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Package;

class UserController extends Controller
{
    use SoftDeletes;

    public function index()
	{
		$agents = User::where('role', 4)->get();

		foreach($agents as $agent)
		{
			$package = Package::find($agent->package_id);
			if(isset($package))
				$agent['package_name'] = $package->name;
			else
				$agent['package_name'] = "None";
		}

		return view('agent', compact('agents'));
	}

	public function edit(User $agent)
	{
		$packages = Package::all();

		return view('edit-agent', compact('agent'), compact('packages'));
	}

	public function update(Request $request)
	{
		$package = $request->get('package');
		$agent = User::find($request->get('user_id'));

		$agent->package_id = $package;
		$agent->save();
		
		return redirect('/agent');
	}
}
