<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionRecord;
use App\User;
use Validator;
use Auth;
use Hash;

class LoginController extends Controller
{
    function index()
    {
    	$records = TransactionRecord::where('user_id', Auth::user()->id)
    	                            ->get();

    	return view('home', compact('records'));
    }

    function checklogin(Request $request)
    {
		$this->validate($request, [
			'email'   => 'required|email',
			'password'  => 'required|alphaNum|min:3'
		]);

		$user_data = array(
			'email'  => $request->get('email'),
			'password' => $request->get('password')
		);

		if(Auth::attempt($user_data))
		{
			return redirect('/home');
		}
		else
		{
			return back()->with('error', 'Wrong Login Details');
		}
    }

    function logout()
    {
		Auth::logout();
		return redirect('/login');
    }

    function register(Request $request)
    {    	
    	$this->validate($request, [
			'creator_id' => 'required',
			'email'   => 'required|email|unique:users,email',
			'name'  => 'required',
			'password'  => 'required|alphaNum|min:3|confirmed',
			'role' => 'required|integer'
		]);
		//

		$user_data = array(
			'creator_id'  => Auth::user()->id,
			'email'  => $request->get('email'),
			'name' => $request->get('name'),
			'password' => Hash::make($request->get('password')),
			'role' => $request->get('role')
		);

		$user = User::create($user_data);
		auth()->login($user);
		return redirect('/home');
    }

    function data(Request $request)
    {
		$html = file_get_contents("https://4dyes.com/getLiveResult.php");
		$result = json_decode($html);
		dd($result);
		
    }
}