<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionRecord;
use App\User;
use App\Package;
use Auth;
use Carbon\Carbon;

class TransactionRecordController extends Controller
{
    public function index()
	{
		$transactions = TransactionRecord::where('user_id', Auth::user()->id)
										 ->whereDate('draw_date', '>=', Carbon::today())
										 ->get();

		$histories = TransactionRecord::where('user_id', Auth::user()->id)
					                  ->whereDate('draw_date', '<', Carbon::today())
					                  ->get();
		
		return view('transaction', compact('transactions'), compact('histories'));
	}

    public function adminIndex()
	{
		$transactions = TransactionRecord::whereDate('draw_date', '>=', Carbon::today())
										 ->get();

		$histories = TransactionRecord::whereDate('draw_date', '<', Carbon::today())
					                  ->get();

		foreach($transactions as $transaction)
		{
			$agent = User::find($transaction->user_id);
			$transaction['agent_name'] = $agent->name;
			$transaction['agent_id'] = $agent->id;
		}

		foreach($histories as $history)
		{
			$agent = User::find($history->user_id);
			$history['agent_name'] = $agent->name;
			$history['agent_id'] = $agent->id;
		}

		return view('admin-transaction', compact('transactions'), compact('histories'));
	}

	public function create(Request $request)
	{
		$this->validate($request, [
			'draw_date'  => 'required',
			'number'   => 'required|numeric|between:0,9999|digits:4',
			'type'  => 'required',
			'amount'  => 'required|numeric|min:0',
			'platform'  => 'required'
		]);

		$package = Package::find(Auth::user()->package_id);

		TransactionRecord::create([
			'draw_date' => $request->get('draw_date'),
			'number' => $request->get('number'),
			'user_id' => Auth::user()->id,
			'type' => $request->get('type'),
			'amount' => $request->get('amount'),
			'platform' => $request->get('platform'),
			'result' => 'Pending',
			'commisson' => ($package->commission_rate * $request->get('amount') / 100),
			'bonus' => 0,
			'settlement' => null
		]);

		return redirect('/transaction');
	}
}
