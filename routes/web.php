<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/new-user', function () {
    return view('new-user');
});

Route::get('/home', 'LoginController@index');
Route::post('/checklogin', 'LoginController@checklogin');
Route::get('/logout', 'LoginController@logout');
Route::post('/register', 'LoginController@register');

Route::get('/agent', 'UserController@index');
Route::get('/edit-agent/{agent}', 'UserController@edit');
Route::post('/update-agent', 'UserController@update');

Route::get('/package', 'PackageController@index');
Route::post('/create-package', 'PackageController@create');
Route::get('/edit-package/{package}', 'PackageController@edit');
Route::post('/update-package', 'PackageController@update');

Route::get('/admin-transaction', 'TransactionRecordController@adminIndex');
Route::get('/transaction', 'TransactionRecordController@index');
Route::post('/create-transaction', 'TransactionRecordController@create');

Route::get('/data', 'LoginController@data');
